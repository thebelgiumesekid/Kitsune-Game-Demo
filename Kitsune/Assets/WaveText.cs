﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveText : MonoBehaviour
{
    Text text;
    public GameObject waveController;
    WaveController waves;
    int waveTotal;
    int currentWave;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        waves = waveController.GetComponent<WaveController>();
        waves.NextWave += UpdateWaveText;
        waveTotal = waves.getTotalWaves();
        UpdateWaveText();
    }

    // Call on wave complete
    void UpdateWaveText()
    {
        currentWave = waves.getCurrentWave() + 1;
        text.text = string.Format("Wave {0}", currentWave);
    }
}

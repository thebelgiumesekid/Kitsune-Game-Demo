﻿using UnityEngine;

public class EndGame : MonoBehaviour {
  // Start is called before the first frame update
  public GameObject player;
  public GameObject fire;
  public GameObject spawnManager;

  public float dayTimer;

  LevelChanger levelChanger;

  void Start() {
    levelChanger = GetComponent<LevelChanger>();

    PlayerDeath playerDeath = player.GetComponent<PlayerDeath>();
    playerDeath.playerDeathComplete += Lose;

    CharacterStats fireStats = fire.GetComponent<CharacterStats>();
    fireStats.OnZeroHealth += Lose;

    WaveController waveController = spawnManager.GetComponent<WaveController>();
    waveController.finalWaveComplete += ChangeSky;
    DayNightManager.instance.OnTransitionComplete += delayWin;
  }

  void Lose() {
    levelChanger.fadeToLevel(2); //deathScreen
  }

  void ChangeSky() {
    //end music
    DayNightManager.instance.StartDay();
  }

  void delayWin() {
    Invoke("Win", dayTimer);
  }

  void Win() {
    levelChanger.fadeToLevel(3); //winScreen
  }
}

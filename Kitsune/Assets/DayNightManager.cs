﻿using UnityEngine;

public class DayNightManager : MonoBehaviour {
  #region Singleton

  public static DayNightManager instance;

  void Awake() {
    if (instance == null) {
      instance = this;
    } else {
      Debug.LogError("Multiple Day night managers.");
      Destroy(this);
    }
  }

  #endregion

  bool isDay = false;
  bool isLerping = false;
  float t = 0f;
  float nextStepTime = 0f;

  public Light sun;
  public float dayIntensity = 1f;
  public float nightIntensity = 0f;
  public float lerpDuration = 10f;
  public float transitionTimeStep = 0f;

  public event System.Action OnTransitionComplete;

  void Start() {
    SetSky(1, 0);
  }

  void Update() {
    if (isLerping && Time.time >= nextStepTime) {
      float intensityLerp;
      float skyboxLerp;

      if (isDay) {
        intensityLerp = Mathf.Lerp(nightIntensity, dayIntensity, t);
        skyboxLerp = Mathf.Lerp(1, 0, t);
      } else {
        intensityLerp = Mathf.Lerp(dayIntensity, nightIntensity, t);
        skyboxLerp = Mathf.Lerp(0, 1, t);
      }
      SetSky(skyboxLerp, intensityLerp);

      float timePassed = Time.time - Mathf.Max(0, nextStepTime - transitionTimeStep);
      t += (timePassed / lerpDuration);
      nextStepTime = Time.time + transitionTimeStep;

      if (t > 1) {
        isLerping = false;
        t = 0f;
        if (OnTransitionComplete != null) {
          OnTransitionComplete();
        }
      }
    }
  }

  void SetSky(float skyboxLerp, float sunLerp) {
    RenderSettings.skybox.SetFloat("_Blend", skyboxLerp);
    sun.intensity = sunLerp;
  }

  void StartTransition() {
    t = 0f;
    nextStepTime = Time.time;
    isLerping = true;
  }

  public void StartDay() {
    if (!isLerping && !isDay) {
      isDay = true;
      StartTransition();
    }
  }

  public void StartNight() {
    if (!isLerping && isDay) {
      isDay = false;
      StartTransition();
    }
  }

  public void Toggle() {
    if (isDay) {
      StartNight();
    } else {
      StartDay();
    }
  }
}

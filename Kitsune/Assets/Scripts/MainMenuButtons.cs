﻿
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour {
  // called first
  void OnEnable()
  {
    //unlock cursor for main menu
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
  }

  public void PlayGame() {
    print("start game");
    SceneManager.LoadScene(1);
  }

  public void QuitGame() {
    print("quit game");
    Application.Quit();
  }

  void OnDisable() {
    //lock cursor for main game
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
  }
}

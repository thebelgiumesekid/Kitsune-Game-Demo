﻿using System.Collections;
using UnityEngine;

public class FireController : MonoBehaviour {
  public GameObject campfireLight;
  public GameObject campfireFire;
  public GameObject campfireSmoke;

  [SerializeField] int fadeDamage = 1;
  [SerializeField] float damageDelay = 1f;

  float fullRange;
  ParticleSystem.EmissionModule fire;
  ParticleSystem.EmissionModule smoke;
  protected CharacterStats stats;
  Light fireLight;
  WaitForSeconds delay;

  void Start() {
    fire = campfireFire.GetComponent<ParticleSystem>().emission;
    smoke = campfireSmoke.GetComponent<ParticleSystem>().emission;
    fireLight = campfireLight.GetComponent<Light>();
    fullRange = fireLight.range;
    delay = new WaitForSeconds(damageDelay);

    stats = GetComponent<CharacterStats>();
    stats.OnTakeDamage += UpdateLight;
    stats.OnHeal += UpdateLight;

    StartCoroutine(FadeLight());
  }

  void UpdateLight() {
    float healthPercent = stats.getHealthPercent();

    fireLight.range = healthPercent * fullRange;
    fire.rateOverTime = healthPercent * 10;
    smoke.rateOverTime = healthPercent * 10;
  }

  IEnumerator FadeLight() {
    while (true) {
      stats.TakeDamage(fadeDamage);
      yield return delay;
    }
  }
}

using UnityEngine;

public class HealthItem : Interactable {
  [SerializeField] int healAmount = 10;

  protected override bool Interact(Transform interactor) {
    CharacterStats stats = interactor.GetComponent<CharacterStats>();
    if (stats != null) {
      print($"Healing {interactor.name} for {healAmount}");
      stats.Heal(healAmount);
      Destroy(this.gameObject);
      return true;
    }

    return false;
  }
}
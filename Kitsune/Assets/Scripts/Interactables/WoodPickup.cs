using UnityEngine;

public class WoodPickup : Pickup {
  [SerializeField] int healAmount = 10;

  public override bool UseOn(GameObject target) {
    if (target.CompareTag("Campfire")) {
      CharacterStats fireStats = target.GetComponent<CharacterStats>();
      fireStats.Heal(healAmount);

      Destroy(this.gameObject);
      return true;
    }

    return false;
  }
}
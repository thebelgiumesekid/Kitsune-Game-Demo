using UnityEngine;

public class Interactable : MonoBehaviour {
  [SerializeField] float radius = 3f;
  [SerializeField] Vector3 interactionPosition = Vector3.zero;

  void OnDrawGizmosSelected() {
    Gizmos.color = Color.yellow;
    Gizmos.DrawWireSphere(transform.position + interactionPosition, radius);
  }

  protected virtual bool Interact(Transform interactor) {
    return true;
  }

  public bool TryInteract(Transform interactor) {
    Vector3 thisPosition = transform.position + interactionPosition;
    if (Vector3.Distance(interactor.position, thisPosition) <= radius) {
      return Interact(interactor);
    }

    return false;
  }

  // public virtual bool UsePickup(Pickup pickup) {
  //   return false;
  // }
}
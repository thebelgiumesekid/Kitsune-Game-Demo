using UnityEngine;

public class Pickup : Interactable {
  Vector3 initialScale;

  public float heldScale = 0.7f;
  public Vector3 holdRotation = Vector3.zero;

  public virtual bool UseOn(GameObject target) { return true; }

  // Interacting with a pickup means picking it up.
  protected override bool Interact(Transform interactor) {
    PlayerInventory inventory = interactor.GetComponent<PlayerInventory>();
    if (inventory == null) {
      return false;
    }

    inventory.HoldItem(this);

    return true;
  }

  public virtual void SetHeld() {
    Rigidbody rb = GetComponent<Rigidbody>();
    if (rb != null) {
      rb.isKinematic = true;
    }

    Collider collider = GetComponent<Collider>();
    if (collider != null) {
      collider.enabled = false;
    }

    initialScale = gameObject.transform.localScale;
    gameObject.transform.localScale *= heldScale;
    gameObject.transform.localRotation = Quaternion.Euler(holdRotation);
  }

  public virtual void UnsetHeld() {
    Rigidbody rb = GetComponent<Rigidbody>();
    if (rb != null) {
      rb.isKinematic = false;
    }

    Collider collider = GetComponent<Collider>();
    if (collider != null) {
      collider.enabled = true;
    }

    gameObject.transform.localScale = initialScale;
  }
}
using UnityEngine;

public class SpiritPickup : Pickup {
  public GameObject allyPrefab;
  public float timeoutTime = 30f;

  void Start() {
    Invoke("Timeout", timeoutTime);
  }

  public override bool UseOn(GameObject target) {
    if (target.CompareTag("Campfire")) {
      GameObject ally = Instantiate(allyPrefab, transform.position, Quaternion.identity);
      // EnemyManager.instance.AddAlly(ally);
      // CharacterStats allyStats = ally.GetComponent<CharacterStats>();
      // allyStats.OnZeroHealth += OnSpawnedDeath;
      SoulDropManager.instance.RemoveSoul();

      Destroy(this.gameObject);
      return true;
    }

    return false;
  }

  // void OnSpawnedDeath()
  // {
  //   SoulDropManager.instance.RemoveSoul();
  // }

  void Timeout() {
    if (this.gameObject != null) {
      SoulDropManager.instance.RemoveSoul();
      Destroy(this.gameObject);
    }
  }
}
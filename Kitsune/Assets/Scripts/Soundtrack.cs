﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Soundtrack : MonoBehaviour {
  AudioSource audioSource;
  AudioClip currentTrack;
  WaitForSeconds fadeFrameTime;
  delegate void FadeCallback();

  public static Soundtrack instance;
  public AudioClip gameWin;
  public float gameWinVolume = 0.15f;
  public AudioClip gamePlaying;
  public float gamePlayingVolume = 0.1f;
  public float fadeTime = 0.5f;

  void Awake() {
    if (instance == null) {
      instance = this;
      DontDestroyOnLoad(this.gameObject);
    } else {
      Destroy(this.gameObject);
    }

    audioSource = GetComponent<AudioSource>();
    fadeFrameTime = new WaitForSeconds(0.1f);
  }

  void OnEnable() {
    SceneManager.sceneLoaded += OnSceneLoaded;
  }

  void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
    if (scene.buildIndex == 0) { // Main menu
      PlayTrack(gameWin, gameWinVolume);
    } else if (scene.buildIndex == 1) { // In game
      PlayTrack(gamePlaying, gamePlayingVolume);
    }

    if (WaveController.instance != null) {
      WaveController.instance.finalWaveComplete += OnWavesComplete;
    }
  }

  void PlayTrack(AudioClip track, float volume) {
    if (currentTrack == track) {
      // Already playing, do nothing
      return;
    }
    print("Playing track");
    StopAllCoroutines();
    if (currentTrack != null) {
      // Something was playing, fade it out first
      currentTrack = track;
      FadeCallback callback = () => PlayTrackCallback(volume);
      StartCoroutine(FadeOut(fadeTime, callback));
    } else {
      // Skip fading, start immediately
      currentTrack = track;
      PlayTrackCallback(volume);
    }
  }

  void PlayTrackCallback(float volume) {
    StartCoroutine(FadeIn(currentTrack, volume, fadeTime));
  }

  void OnWavesComplete() {
    PlayTrack(gameWin, gameWinVolume);
  }

  IEnumerator FadeIn(AudioClip track, float volume, float fadeTime, FadeCallback callback = null) {
    audioSource.volume = 0;
    audioSource.clip = track;
    audioSource.Play();
    float lastTime = Time.time;

    while (audioSource.volume < volume) {
      float deltaTime = Time.time - lastTime;
      float newVolume = audioSource.volume;
      newVolume += (deltaTime / fadeTime) * volume;
      audioSource.volume = Mathf.Clamp(newVolume, 0, volume);
      lastTime = Time.time;

      yield return fadeFrameTime;
    }

    audioSource.volume = volume;
    if (callback != null) {
      callback();
    }
  }

  IEnumerator FadeOut(float fadeTime, FadeCallback callback = null) {
    float lastTime = Time.time;
    float startVolume = audioSource.volume;

    while (audioSource.volume > 0) {
      float deltaTime = Time.time - lastTime;
      float newVolume = audioSource.volume;
      newVolume -= (deltaTime / fadeTime) * startVolume;
      audioSource.volume = Mathf.Clamp01(newVolume);
      lastTime = Time.time;

      yield return fadeFrameTime;
    }

    audioSource.volume = 0;
    audioSource.Stop();
    if (callback != null) {
      callback();
    }
  }
}

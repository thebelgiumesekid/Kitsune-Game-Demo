﻿using UnityEngine;

public class CharacterStats : MonoBehaviour {
  public int maxHealth = 1;
  public int currentHealth { get; protected set; }
  public int damage;

  public event System.Action OnZeroHealth;
  public event System.Action OnTakeDamage;
  public event System.Action OnHeal;

  void Awake() {
    currentHealth = maxHealth;
  }

  public void TakeDamage(int damageTaken) {
    int newHealth = currentHealth - damageTaken;
    int prevHealth = currentHealth;
    currentHealth = Mathf.Max(0, newHealth);

    if (OnTakeDamage != null && prevHealth > 0) {
      OnTakeDamage();
    }

    if (prevHealth > 0 && currentHealth <= 0 && OnZeroHealth != null) {
      OnZeroHealth();
    }
  }

  public void Heal(int amount) {
    int newHealth = currentHealth + amount;
    currentHealth = Mathf.Min(newHealth, maxHealth);
    OnHeal();
  }

  public float getHealthPercent() {
    return (float)currentHealth / (float)maxHealth;
  }
}

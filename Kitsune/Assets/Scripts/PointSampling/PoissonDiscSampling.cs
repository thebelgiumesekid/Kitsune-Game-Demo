using System.Collections.Generic;
using UnityEngine;

public static class PoissonDiscSampling {
  public static List<Vector2> GeneratePoints(float radius, Vector2 regionSize, List<Vector2> existingPoints) {
    float cellSize = radius / Mathf.Sqrt(2);
    int gridXSize = Mathf.CeilToInt(regionSize.x / cellSize);
    int gridYSize = Mathf.CeilToInt(regionSize.y / cellSize);
    int[,] grid = new int[gridXSize, gridYSize];
    List<Vector2> points = new List<Vector2>();
    List<Vector2> spawnPoints = new List<Vector2>();

    spawnPoints.Add(regionSize / 2);
    while (spawnPoints.Count > 0) {
      int spawnIndex = Random.Range(0, spawnPoints.Count);
      Vector2 spawnPoint = spawnPoints[spawnIndex];


    }

    return points;
  }
}
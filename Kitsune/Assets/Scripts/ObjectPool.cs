using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {
  Dictionary<string, SingleSpawnPool> pools;

  public static ObjectPool instance;

  void Awake() {
    HandleSingleton();
    pools = new Dictionary<string, SingleSpawnPool>();
  }

  void HandleSingleton() {
    if (instance == null) {
      instance = this;
    } else {
      Debug.LogError("Tried to instantiate multiple SpawnPools.");
    }
  }

  SingleSpawnPool TryGetPoolFromName(string poolName) {
    SingleSpawnPool pool;
    bool validName = pools.TryGetValue(poolName, out pool);
    if (!validName) {
      Debug.LogError($"{poolName} is not a valid pool name.");
      return null;
    }
    return pool;
  }

  public bool CreatePool(string poolName, GameObject prefab, ushort amount) {
    if (pools.ContainsKey(poolName)) {
      Debug.LogError($"Pool {poolName} already exists.");
      return false;
    }

    pools.Add(poolName, new SingleSpawnPool(prefab, amount));
    return true;
  }

  public GameObject GetFromPool(string poolName) {
    SingleSpawnPool pool = TryGetPoolFromName(poolName);
    if (pool == null) {
      return null;
    }

    return pool.GetFromPool();
  }

  public class SingleSpawnPool {
    Queue<GameObject> queue;

    public SingleSpawnPool(GameObject prefab, ushort amount) {
      queue = new Queue<GameObject>(amount);

      for (ushort i = 0; i < amount; i++) {
        GameObject newObj = Instantiate(prefab);
        newObj.SetActive(false);

        queue.Enqueue(newObj);
      }
    }

    public GameObject GetFromPool() {
      if (queue.Count == 0) {
        return null;
      }

      GameObject obj = queue.Dequeue();

      IPooledObject pooledObj = obj.GetComponent<IPooledObject>();
      if (pooledObj != null) {
        pooledObj.OnObjectSpawn();
      }

      queue.Enqueue(obj);
      obj.SetActive(true);

      return obj;
    }
  }
}
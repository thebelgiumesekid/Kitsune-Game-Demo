﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
  Animator playerAnim;
  public event System.Action playerDeathComplete;

  // Start is called before the first frame update
  void Start()
  {
    //lose if player health is at zero
    CharacterStats playerStats = GetComponent<CharacterStats>();
    playerAnim = GetComponent<Animator>();
    playerStats.OnZeroHealth += initiateLoss;
  }

  void initiateLoss()
  {
    print("Game Over");
    TriggerPlayerDieAnim();
  }


  void TriggerPlayerDieAnim()
  {
    //death animation
    playerAnim.SetInteger("State", 10);
    //levelChanger.fadeToLevel(2); //deathScreen
  }

  public void FinishDieEvent() {
    playerDeathComplete();
  } 
}

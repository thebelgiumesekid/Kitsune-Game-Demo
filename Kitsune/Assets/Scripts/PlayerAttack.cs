﻿using UnityEngine;

[RequireComponent(typeof(HitBoxCombat))]
public class PlayerAttack : MonoBehaviour {
  HitBoxCombat combat;

  void Awake() {
    combat = GetComponent<HitBoxCombat>();
  }

  void Update() {
    if (Input.GetKeyDown(KeyCode.Mouse0)) {
      ProcessAttack();
    }
  }

  bool CanHitWithTag(string tag) {
    return tag == "Enemy";
  }

  void ProcessAttack() {
    Collider[] hitByAttack = combat.GetHitByAttack();

    foreach (Collider targetCollider in hitByAttack) {
      if (CanHitWithTag(targetCollider.gameObject.tag)) {
        CharacterStats targetStats = targetCollider.GetComponent<CharacterStats>();
        if (targetStats != null) {
          combat.AttackImmediate(targetStats);
        }
      }
    }
  }

  public void AttackHitEvent() {
    return;
  }
}

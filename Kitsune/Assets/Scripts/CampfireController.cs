﻿using UnityEngine;
using System.Collections.Generic;

public class CampfireController : MonoBehaviour {
  List<Transform> enemiesInRange;

  void Awake() {
    enemiesInRange = new List<Transform>();
  }

  void OnTriggerEnter(Collider other) {
    if (other.CompareTag("Enemy")) {
      enemiesInRange.Add(other.transform);
    }
  }

  void onTriggerLeave(Collider other) {
    if (other.CompareTag("Enemy")) {
      enemiesInRange.Remove(other.transform);
    }
  }

  public List<Transform> GetEnemiesInRange() {
    return enemiesInRange;
  }
}
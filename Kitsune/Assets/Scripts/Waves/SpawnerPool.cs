using UnityEngine;
using System.Collections.Generic;

/* Contains information for what a spawner should spawn in a wave */

[CreateAssetMenu(fileName = "SpawnPool", menuName = "ScriptableObjects/SpawnerPool", order = 1)]
public class SpawnerPool : ScriptableObject {
  [System.Serializable]
  public struct WaveObject {
    public GameObject prefab;
    public int amount;
  };

  public List<WaveObject> prefabTypes;
}
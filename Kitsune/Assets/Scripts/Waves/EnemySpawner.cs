using UnityEngine;

public class EnemySpawner : Spawner {
  protected override GameObject SpawnPrefab(GameObject prefab) {
    GameObject newEnemy = base.SpawnPrefab(prefab);
    newEnemy.name = "Enemy " + EnemyManager.instance.enemies.Count;
    // EnemyManager.instance.AddEnemy(newEnemy);
    return newEnemy;
  }
}
﻿using System.Collections.Generic;
using UnityEngine;

/* Controls a single spawner, only spawns when triggered */

public class Spawner : MonoBehaviour {
  public Vector3 size = Vector3.one;
  public float minDistanceBetween = 1.5f;
  public float maxSpawnAttempts = 5;

  void OnDrawGizmosSelected() {
    Gizmos.color = new Color(1, 0, 0, 0.7f);
    Gizmos.DrawWireCube(transform.position, size);
  }

  public void SpawnWavePool(SpawnerPool pool) {
    foreach (SpawnerPool.WaveObject enemyData in pool.prefabTypes) {
      SpawnPrefabs(enemyData.prefab, enemyData.amount);
    }
  }

  public void SpawnPrefabs(GameObject prefab, int amount) {
    for (int i = 0; i < amount; i++) {
      SpawnPrefab(prefab);
    }
  }

  protected virtual GameObject SpawnPrefab(GameObject prefab) {
    Vector3 pos = GetSpawnPosition();
    GameObject newInstance = Instantiate(prefab, pos, Quaternion.identity, transform);
    return newInstance;
  }

  bool isValidSpawnPosition(Transform[] existingSpawns, Vector3 newPos) {
    for (int i = 1; i < existingSpawns.Length; i++) {
      Transform existingSpawn = existingSpawns[i];
      float distance = Vector3.Distance(existingSpawn.position, newPos);
      if (distance < minDistanceBetween) {
        return false;
      }
    }

    return true;
  }

  Vector3 GetSpawnPosition() {
    Transform[] existingSpawns = transform.GetComponentsInChildren<Transform>();
    Vector3 newPos = transform.position + GetRandomPosition();
    int i = 0;

    // Try to find a non-colliding spawn position
    while (i < maxSpawnAttempts) {
      if (isValidSpawnPosition(existingSpawns, newPos)) {
        return newPos;
      }
      newPos = transform.position + GetRandomPosition();
      i++;
    }

    // Failed to find non-colliding spawn position, use anyway
    return newPos;
  }

  Vector3 GetRandomPosition() {
    return new Vector3(
      Random.Range(-size.x / 2, size.x / 2),
      Random.Range(-size.y / 2, size.y / 2),
      Random.Range(-size.z / 2, size.z / 2)
    );
  }
}

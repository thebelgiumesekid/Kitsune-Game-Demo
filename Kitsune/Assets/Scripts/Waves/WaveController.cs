using UnityEngine;
using System.Collections.Generic;
using System;

/* Key-Value pair for what a single spawner spawns in a wave */
[Serializable]
public struct WaveSpawnerGroup {
  public SpawnerPool pool;
  public Spawner spawner;
};

/* List of all spawners and groups to during a wave */
[Serializable]
public struct Wave {
  public List<WaveSpawnerGroup> spawnGroups;
  public float predelay;
};

public class WaveController : MonoBehaviour {
  #region singleton
  public static WaveController instance;

  void Awake() {
    if (instance == null) {
      instance = this;
    } else {
      Debug.LogError("Multiple instances of singleton");
      Destroy(this);
    }
  }
  #endregion

  [SerializeField] List<Wave> waves = null;
  int waveIndex = 0;
  public event System.Action NextWave;
  public event System.Action finalWaveComplete;

  void Start() {
    EnemyManager.instance.onWaveComplete += onWaveComplete;
    WaitThenSpawnWave();
  }

  void onWaveComplete() {
    if (waveIndex == waves.Count) {
      finalWaveComplete();
    } else {
      WaitThenSpawnWave();
    }
  }

  void WaitThenSpawnWave() {
    if (waves != null) {
      Invoke("SpawnNextWave", waves[waveIndex].predelay);
    }
  }

  void SpawnNextWave() {
    if (NextWave != null) {
      NextWave();
    }

    Wave wave = waves[waveIndex];
    foreach (WaveSpawnerGroup group in wave.spawnGroups) {
      group.spawner.SpawnWavePool(group.pool);
    }

    waveIndex++;
  }

  public int getTotalWaves() {
    return waves.Count;
  }

  public int getCurrentWave() {
    return waveIndex;
  }
}
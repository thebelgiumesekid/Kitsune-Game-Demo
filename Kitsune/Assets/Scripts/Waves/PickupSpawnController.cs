using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public struct PickupGroup {
  public Spawner spawner;
  public int apples;
  public int wood;
}

[Serializable]
public struct PickupWave {
  public List<PickupGroup> spawnGroups;
};

public class PickupSpawnController : MonoBehaviour {
  [SerializeField] GameObject applePrefab = null;
  [SerializeField] GameObject woodPrefab = null;
  [SerializeField] List<PickupWave> pickupWaves = null;
  int waveIndex = 0;

  void Start() {
    EnemyManager.instance.onWaveComplete += onWaveComplete;
    onWaveComplete();
  }

  void onWaveComplete() {
    SpawnPickups();
  }

  void SpawnPickups() {
    if (pickupWaves != null && waveIndex < pickupWaves.Count) {
      PickupWave wave = pickupWaves[waveIndex];
      foreach (PickupGroup group in wave.spawnGroups) {
        TrySpawnPickups(group.spawner, applePrefab, group.apples);
        TrySpawnPickups(group.spawner, woodPrefab, group.wood);
      }
      waveIndex++;
    }
  }

  void TrySpawnPickups(Spawner spawner, GameObject prefab, int amount) {
    if (amount > 0 && prefab != null && spawner != null) {
      spawner.SpawnPrefabs(prefab, amount);
    }
  }
}
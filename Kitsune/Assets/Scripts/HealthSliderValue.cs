﻿using UnityEngine;
using UnityEngine.UI;

public class HealthSliderValue : MonoBehaviour
{
  protected CharacterStats stats;
  Slider slider;
  
  public GameObject healthOwner;

    // Start is called before the first frame update
    void Start()
    {
        stats = healthOwner.GetComponent<CharacterStats>();
        slider = GetComponent<Slider>();
        stats.OnTakeDamage += UpdateSlider;
        stats.OnHeal += UpdateSlider;
    }

    // Update is called once per frame
    void UpdateSlider()
    {
        slider.value = stats.getHealthPercent();
    }
}

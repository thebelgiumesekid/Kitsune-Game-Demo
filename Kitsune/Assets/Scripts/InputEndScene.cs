﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputEndScene : MonoBehaviour
{
    public LevelChanger levelChanger;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)) {
            levelChanger.fadeToLevel(0);
        }
    }
}

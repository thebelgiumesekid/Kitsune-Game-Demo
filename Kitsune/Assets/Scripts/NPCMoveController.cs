﻿using UnityEngine;
using UnityEngine.AI;

/* Base NPC Logic to follow a target */

public enum NPCMoveStatus {
  Complete, // Has finished moving and is in range
  Incomplete, // Has finished moving and is NOT within range
  InProgress // Is still moving
};

[RequireComponent(typeof(NavMeshAgent))]
public class NPCMoveController : MonoBehaviour {
  NavMeshAgent agent;
  Transform target;
  bool isPaused = false;

  [SerializeField] float rotateSpeed = 5f;
  [SerializeField] float distanceBuffer = 1f;
  [SerializeField] float angleBuffer = 10f;

  public delegate void OnTargetInteract(Transform target);
  public OnTargetInteract onTargetInteractCallback;

  void Awake() {
    agent = GetComponent<NavMeshAgent>();
  }

  void Update() {
    if (!isPaused) {
      MoveTowardsTarget();
    }
  }

  void OnDrawGizmosSelected() {
    DrawTarget();
  }

  public void MoveToPoint(Vector3 point) {
    agent.SetDestination(point);
    agent.isStopped = false;
  }

  // public void FollowTarget(Transform newTarget, bool isMoving) {
  //   FollowTarget(newTarget);
  //   targetIsMoving = isMoving;
  // }

  public void FollowTarget(Transform newTarget) {
    target = newTarget;
    agent.isStopped = false;
    agent.SetDestination(target.position);
  }

  public void StopFollowTarget() {
    target = null;
    agent.isStopped = true;
  }

  public NPCMoveStatus GetPathStatus() {
    if (agent.pathStatus == NavMeshPathStatus.PathInvalid
        || agent.pathStatus == NavMeshPathStatus.PathPartial) {
      return NPCMoveStatus.Incomplete;
    }

    if (!isMoving()) {
      // Agent has stopped
      float distance = Vector3.Distance(target.position, transform.position);
      if (distance <= agent.stoppingDistance) {
        return NPCMoveStatus.Complete;
      }
      return NPCMoveStatus.Incomplete;
    }

    // Agent is still moving
    return NPCMoveStatus.InProgress;
  }

  public bool isMoving() {
    return agent.velocity.sqrMagnitude >= Mathf.Epsilon;
  }

  private void MoveTowardsTarget() {
    if (target != null) {
      float distance = Vector3.Distance(target.position, transform.position);
      if ((distance - distanceBuffer) < agent.stoppingDistance) {
        RotateTowardsTarget();
        if (onTargetInteractCallback != null && IsFacingTarget()) {
          onTargetInteractCallback(target);
        }
      } else {
        agent.SetDestination(target.position);
        agent.isStopped = false;
      }
    }
  }

  Quaternion GetAngleToTarget() {
    Vector3 lookPos = target.position - transform.position;
    lookPos.y = 0;
    return Quaternion.LookRotation(lookPos);
  }

  void RotateTowardsTarget() {
    Quaternion rotation = GetAngleToTarget();
    float dampFactor = Time.deltaTime * rotateSpeed;

    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, dampFactor);
  }

  bool IsFacingTarget() {
    Quaternion targetDirection = GetAngleToTarget();
    Quaternion currentDirection = transform.rotation;
    return (Quaternion.Angle(targetDirection, currentDirection) <= angleBuffer);
  }

  void DrawTarget() {
    if (target != null) {
      Gizmos.color = Color.cyan;
      Gizmos.DrawLine(transform.position, target.position);
    }
  }

  public void SetPaused(bool _isPaused) {
    agent.isStopped = isPaused;
    isPaused = _isPaused;
  }
}

﻿using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour {
  CharacterStats stats;
  float nextAttack = 0f;

  public float attackRate = 1f;
  public delegate void OnStartAttackEvent(CharacterStats target);
  public OnStartAttackEvent OnStartAttack;

  void Awake() {
    stats = GetComponent<CharacterStats>();
  }

  public bool Attack(CharacterStats enemy) {
    float curTime = Time.time;
    if (curTime > nextAttack) {
      nextAttack = curTime + attackRate;

      if (OnStartAttack != null) {
        OnStartAttack(enemy);
      }
      return true;
    }
    return false;
  }

  public bool AttackImmediate(CharacterStats enemy) {
    float curTime = Time.time;
    if (curTime > nextAttack) {
      nextAttack = curTime + attackRate;

      if (OnStartAttack != null) {
        OnStartAttack(enemy);
      }
      this.Damage(enemy);

      return true;
    }
    return false;
  }

  public void Damage(CharacterStats enemy) {
    enemy.TakeDamage(stats.damage);
  }
}

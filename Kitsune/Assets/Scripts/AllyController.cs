﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Controls an Ally's movement, attacks */

[RequireComponent(typeof(CharacterCombat))]
[RequireComponent(typeof(NPCMoveController))]
public class AllyController : MonoBehaviour {
  NPCMoveController moveController;
  Transform currentTarget;
  CharacterCombat combat;
  UnityEngine.WaitForSeconds updateTargetDelay;
  List<GameObject> enemies;

  [SerializeField] float updateTargetRate = 0.5f;
  [SerializeField] float targetSwitchDistance = 2f;
  [SerializeField] float maxDistanceFromCampfire = 10;
  [SerializeField] Transform campfire = null;

  void Awake() {
    combat = GetComponent<CharacterCombat>();
    moveController = GetComponent<NPCMoveController>();
    updateTargetDelay = new WaitForSeconds(updateTargetRate);
  }

  void Start() {
    enemies = EnemyManager.instance.enemies;
    EnemyManager.instance.AddAlly(this.gameObject);
    moveController.onTargetInteractCallback += Attack;
    FindCampfire();
    StartCoroutine(UpdateTarget());
  }

  void OnDrawGizmosSelected() {
    if (campfire != null) {
      Gizmos.color = Color.green;
      Gizmos.DrawWireSphere(campfire.position, maxDistanceFromCampfire);
    }
  }

  void FindCampfire() {
    if (campfire == null) {
      campfire = GameObject.FindGameObjectWithTag("Campfire").transform;
    }
  }

  IEnumerator UpdateTarget() {
    while (true) {
      ChooseTarget();
      yield return updateTargetDelay;
    }
  }

  void ChooseTarget() {
    if (campfire == null) {
      return;
    }

    Transform closestEnemy = GetClosestEnemy();
    if (currentTarget == null) {
      SetTarget(closestEnemy);
      return;
    } else if (closestEnemy == currentTarget) {
      return;
    }


    float curTargetDis = Vector3.SqrMagnitude(transform.position - currentTarget.position);
    float closestEnemyDis = Vector3.SqrMagnitude(transform.position - closestEnemy.position);
    if (Mathf.Abs(curTargetDis - closestEnemyDis) >= targetSwitchDistance) {
      SetTarget(closestEnemy);
    }
  }

  Transform GetClosestEnemy() {
    float minDistance = Mathf.Infinity;
    Transform closest = null;

    foreach (GameObject enemy in enemies) {
      float disToCampfire = Vector3.Magnitude(enemy.transform.position - campfire.position);
      if (disToCampfire > maxDistanceFromCampfire) {
        continue;
      }
      float distance = Vector3.SqrMagnitude(transform.position - enemy.transform.position);
      if (distance < minDistance) {
        minDistance = distance;
        closest = enemy.transform;
      }
    }
    return closest;
  }

  void SetTarget(Transform newTarget) {
    if (currentTarget != newTarget) {
      moveController.FollowTarget(newTarget);
      currentTarget = newTarget;
    }
  }

  void Attack(Transform target) {
    if (target == currentTarget) {
      CharacterStats targetStats = target.gameObject.GetComponent<CharacterStats>();
      if (targetStats != null) {
        combat.Attack(targetStats);
      }
    }
  }

  public void Die() {
    EnemyManager.instance.RemoveAlly(this.gameObject);
    Destroy(this.gameObject);
  }

  public void AttackAt(Transform target) {
    if (target == null) {
      return; // Target might have died before attack triggers
    }

    CharacterStats stats = target.gameObject.GetComponent<CharacterStats>();
    if (stats != null) {
      combat.Damage(stats);
    }
  }
}

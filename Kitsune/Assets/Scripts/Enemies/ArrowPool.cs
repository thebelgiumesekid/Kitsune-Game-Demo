﻿using System.Collections.Generic;
using UnityEngine;

public class ArrowPool : MonoBehaviour {
  // List<GameObject> pool;
  Queue<GameObject> pool;

  [SerializeField] ushort initialSize = 100;
  [SerializeField] GameObject prefab = null;

  public static ArrowPool instance;

  void Awake() {
    HandleSingleton();
    pool = new Queue<GameObject>(initialSize);
    // pool = new List<GameObject>();
  }

  void Start() {
    for (ushort i = 0; i < initialSize; i++) {
      GameObject instance = Instantiate(prefab, this.transform);
      instance.SetActive(false);
      pool.Enqueue(instance);
    }
  }

  void HandleSingleton() {
    if (instance == null) {
      instance = this;
    } else {
      Debug.LogError("Tried to instantiate multiple SpawnPools.");
    }
  }

  // Get from the pool. If empty, reuse the oldest active one
  // public GameObject GetFromPool() {
  //   for (ushort i = 0; i < pool.Count; i++) {
  //     if (!pool[i].activeInHierarchy) {
  //       return pool[i];
  //     }
  //   }

  //   // No inactive items. Return the first active item.
  //   GameObject firstActive = pool[0];
  //   pool.Remove(firstActive);
  //   pool.Add(firstActive);
  //   return firstActive;
  // }

  GameObject GetFromQueue() {
    GameObject obj = pool.Dequeue();
    // obj.SetActive(true);

    IPooledObject pooledObj = obj.GetComponent<IPooledObject>();
    if (pooledObj != null) {
      pooledObj.OnObjectSpawn();
    }

    return obj;
  }

  public GameObject GetFromPool() {
    if (pool.Count == 0) {
      return Instantiate(prefab);
    } else {
      return GetFromQueue();
    }
  }

  public GameObject GetFromPool(Vector3 position, Quaternion rotation) {
    if (pool.Count == 0) {
      return Instantiate(prefab, position, rotation);
    } else {
      GameObject arrow = GetFromQueue();
      arrow.transform.position = position;
      arrow.transform.rotation = rotation;

      return arrow;
    }
  }

  public GameObject GetFromPool(Vector3 position, Quaternion rotation, Transform parent) {
    if (pool.Count == 0) {
      return Instantiate(prefab, position, rotation, parent);
    } else {
      GameObject arrow = GetFromQueue();
      arrow.transform.position = position;
      arrow.transform.rotation = rotation;
      arrow.transform.parent = parent;

      return arrow;
    }
  }

  // // Return to pool. 
  public void ReturnToPool(GameObject obj) {
    obj.SetActive(false);
    pool.Enqueue(obj);
  }
}

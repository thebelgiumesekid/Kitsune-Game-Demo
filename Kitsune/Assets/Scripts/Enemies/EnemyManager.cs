﻿using UnityEngine;
using System.Collections.Generic;

/* Stores all existing enemies for Ally AI and wave logic */

public class EnemyManager : MonoBehaviour {
  #region Singleton

  public static EnemyManager instance;

  void Awake() {
    if (instance == null) {
      instance = this;
    } else {
      Debug.LogError("Multiple enemy managers detected");
      Destroy(this);
    }
    enemies = new List<GameObject>();
    allies = new List<GameObject>();
  }

  #endregion

  public List<GameObject> enemies { get; protected set; }
  public List<GameObject> allies { get; protected set; }
  public event System.Action onWaveComplete;

  public void AddEnemy(GameObject enemy) {
    enemies.Add(enemy);
  }

  public void RemoveEnemy(GameObject enemy) {
    enemies.Remove(enemy);
    if (enemies.Count == 0 && onWaveComplete != null) {
      onWaveComplete();
    }
  }

  public void AddAlly(GameObject ally) {
    allies.Add(ally);
  }

  public void RemoveAlly(GameObject ally) {
    allies.Remove(ally);
  }
}
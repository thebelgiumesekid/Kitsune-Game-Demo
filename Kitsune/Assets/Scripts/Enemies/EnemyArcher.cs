using System;
using UnityEngine;

/* Main controller for the archer enemy */

public class EnemyArcher : EnemyController {
  GameObject arrowParent;

  [SerializeField] float arrowSpeed = 10f;
  [SerializeField] Vector3 arrowSpawn = Vector3.zero;

  public Arrow arrowPrefab;

  AudioSource source;
  public AudioClip attackSound;

  protected override void Awake() {
    base.Awake();
    arrowParent = GameObject.FindGameObjectWithTag("SpawnedObjects");
    source = GetComponent<AudioSource>();
  }

  void OnDrawGizmosSelected() {
    Gizmos.color = Color.green;
    Gizmos.DrawWireSphere(arrowSpawn, 0.1f);
  }

  public override void AttackAt(Transform target) {
    base.AttackAt(target);
    if (arrowPrefab == null) {
      return;
    }

    source.PlayOneShot(attackSound);
    ShootArrowAt(target);
  }

  float GetProjectileAngle(float distance, float speed, float yOffset) {
    float gravity = Physics.gravity.magnitude;

    float factor = (-0.5f * gravity * Mathf.Pow(distance, 2)) / Mathf.Pow(speed, 2);
    float top = (-distance + Mathf.Sqrt(Mathf.Pow(distance, 2) - 4 * factor * (yOffset + factor)));
    float bottom = (2 * factor);
    return Mathf.Atan(top / bottom);
  }

  float GetProjectileVelocity(float distance, float angle, float yOffset) {
    float gravity = Physics.gravity.magnitude;

    return (1 / Mathf.Cos(angle))
        * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) /
          (distance * Mathf.Tan(angle) + yOffset));
  }

  GameObject ShootArrowAt(Transform target) {
    Vector3 startPosition = transform.TransformPoint(arrowSpawn);
    Vector3 noYOffset = target.position - startPosition;
    float yOffset = -noYOffset.y - 0.3f; // Offset to not shoot at bottom of object
    // print(yOffset);
    noYOffset.y = 0;
    float distance = noYOffset.magnitude;
    float velocity = arrowSpeed;

    float angle = GetProjectileAngle(distance, arrowSpeed, yOffset);
    if (float.IsNaN(angle)) {
      angle = 0.4f; // Hard-code backup angle
      print("ANGLE BROKEN");
      velocity = GetProjectileVelocity(distance, angle, yOffset);
    }

    float degrees = Mathf.Rad2Deg * angle;
    Quaternion lookRotation = Quaternion.LookRotation(noYOffset) * Quaternion.Euler(-degrees, 0, 0);

    GameObject arrow = Instantiate(
      arrowPrefab.gameObject,
      startPosition,
      lookRotation,
      arrowParent.transform
    );

    Arrow arrowScript = arrow.GetComponent<Arrow>();
    arrowScript.SetVelocity(velocity);
    arrowScript.onArrowHitCallback += ArrowHitCallback;

    return arrow;
  }

  void ArrowHitCallback(Collider collider, Arrow arrow) {
    string tag = collider.gameObject.tag;

    if (CanHitWithTag(tag)) {
      CharacterStats hitStats = collider.gameObject.GetComponent<CharacterStats>();
      if (hitStats != null) {
        combat.Damage(hitStats);
        arrow.Destroy();
      }
    } else if (!CanIgnoreWithTag(tag)) {
      arrow.Stick();
    }
  }
}
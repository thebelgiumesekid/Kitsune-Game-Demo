﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Controls an Ally's movement, attacks */

[RequireComponent(typeof(CharacterCombat))]
[RequireComponent(typeof(NPCMoveController))]
public class EnemyController : MonoBehaviour {

  protected NPCMoveController moveController;
  Transform currentTarget;
  UnityEngine.WaitForSeconds updateTargetDelay;
  protected CharacterCombat combat;
  List<GameObject> allies;

  [SerializeField] float alternateTriggerDistance = 10f;
  [SerializeField] float updateTargetRate = 0.5f;
  [SerializeField] float playerPriorityDistance = 1f;

  public Transform defaultTarget;
  public Transform alternateTarget;
  public event System.Action OnDestroy;

  protected virtual void Awake() {
    InitComponents();
    updateTargetDelay = new WaitForSeconds(updateTargetRate);
  }

  protected virtual void Start() {
    InitTargets();

    allies = EnemyManager.instance.allies;
    EnemyManager.instance.AddEnemy(this.gameObject);

    //this needs to be called after the death animation is played.
    moveController.FollowTarget(defaultTarget);
    moveController.onTargetInteractCallback += StartAttack;

    StartCoroutine(UpdateTarget());
  }

  void InitComponents() {
    moveController = GetComponent<NPCMoveController>();
    combat = GetComponent<CharacterCombat>();
  }

  void InitTargets() {
    defaultTarget = GameObject.FindWithTag("Campfire").transform;
    alternateTarget = GameObject.FindWithTag("Player").transform;
  }

  IEnumerator UpdateTarget() {
    while (true) {
      ChooseTarget();
      yield return updateTargetDelay;
    }
  }

  void ChooseTarget() {
    // Check distance to alternate target (player)
    float targetDistance = float.MaxValue;
    if (alternateTarget != null) {
      targetDistance = Vector3.Distance(alternateTarget.position, transform.position);
    }

    // Find closest ally
    float closestAllyDistance = float.MaxValue;
    GameObject closestAlly = null;
    foreach (GameObject ally in allies) {
      float distance = Vector3.Distance(ally.transform.position, transform.position);
      if (distance < closestAllyDistance) {
        closestAllyDistance = distance;
        closestAlly = ally;
      }
    }

    if (targetDistance <= alternateTriggerDistance) {
      // Player is in range
      if ((targetDistance - closestAllyDistance) >= playerPriorityDistance) {
        // Ally is closer, attack it instead
        SetTarget(closestAlly.transform);
      } else {
        SetTarget(alternateTarget);
      }
    } else if (closestAllyDistance <= alternateTriggerDistance) {
      SetTarget(closestAlly.transform);
    } else {
      SetTarget(defaultTarget);
    }
  }

  void SetTarget(Transform newTarget) {
    if (currentTarget != newTarget) {
      moveController.FollowTarget(newTarget);
      currentTarget = newTarget;
    }
  }

  protected virtual void StartAttack(Transform target) {
    CharacterStats targetStats = target.GetComponent<CharacterStats>();
    combat.Attack(targetStats);
  }

  public virtual void AttackAt(Transform target) { }

  protected bool CanHitWithTag(string tag) {
    return (tag == "Ally" || tag == "Campfire" || tag == "Player");
  }

  protected bool CanIgnoreWithTag(string tag) {
    return (tag == "Enemy");
  }

  public void Die() {
    if (OnDestroy != null) {
      OnDestroy();
    }
    EnemyManager.instance.RemoveEnemy(this.gameObject);
    Destroy(this.gameObject);
  }
}

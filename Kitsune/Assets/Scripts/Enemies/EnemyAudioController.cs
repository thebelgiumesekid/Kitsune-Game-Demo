﻿using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
[RequireComponent(typeof(AudioSource))]
public class EnemyAudioController : MonoBehaviour {
  AudioSource source;
  public AudioClip takeDamageSound;

  void Awake() {
    source = GetComponent<AudioSource>();
  }

  void Start() {
    CharacterStats stats = GetComponent<CharacterStats>();
    stats.OnTakeDamage += OnTakeDamage;
  }

  void OnTakeDamage() {
    if (takeDamageSound != null) {
      source.PlayOneShot(takeDamageSound);
    }
  }
}

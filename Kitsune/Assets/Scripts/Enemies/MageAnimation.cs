﻿
public class MageAnimation : EnemyAnimations {
  protected override void Awake() {
    base.Awake();
    EnemyMage mage = GetComponentInParent<EnemyMage>();
    mage.OnAttackFinish += FinishAttack;
  }

  public void FinishAttack() {
    animator.SetTrigger("FinishAttack");
  }
}

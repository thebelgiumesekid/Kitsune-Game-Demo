﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyAnimations : MonoBehaviour {
  NavMeshAgent agent;
  EnemyController controller;
  Transform target;

  protected CharacterStats stats;
  protected Animator animator;

  protected virtual void Awake() {
    InitComponents();
  }

  // Start is called before the first frame update
  void Start() {
    CharacterStats stats = GetComponentInParent<CharacterStats>();
    stats.OnZeroHealth += TriggerDie;

    CharacterCombat combat = GetComponentInParent<CharacterCombat>();
    combat.OnStartAttack += StartAttack;

    controller = GetComponentInParent<EnemyController>();
  }

  void Update() {
    //transition from idle to sprint
    float speedPercent = agent.velocity.magnitude / agent.speed;
    animator.SetFloat("SpeedPercent", speedPercent, .1f, Time.deltaTime);
  }

  void InitComponents() {
    animator = GetComponent<Animator>();
    agent = GetComponentInParent<NavMeshAgent>();
    stats = GetComponent<CharacterStats>();
  }

  void StartAttack(CharacterStats target) {
    animator.SetTrigger("Attack");
    this.target = target.transform;
  }

  void TriggerDie() {
    animator.SetTrigger("Die");
  }

  public void FinishDieEvent() {
    controller.Die();
  }

  public void AttackHitEvent() {
    controller.AttackAt(target);
  }
}

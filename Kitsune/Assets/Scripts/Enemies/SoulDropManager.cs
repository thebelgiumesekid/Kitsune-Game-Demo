﻿using UnityEngine;

/* Keeps track of all souls and allies, calculates chance for spawing */

public class SoulDropManager : MonoBehaviour {
  #region Singleton

  public static SoulDropManager instance;

  void AwakeSingleton() {
    if (instance == null) {
      instance = this;
    } else {
      Debug.LogError("Multiple Soul Drop Controllers detected");
      Destroy(this);
    }
  }
  #endregion}

  int soulCount = 0;

  public int maxSoulsAtOnce = 3; // Includes spawned allies
  [Range(0f, 1f)]
  public float initialChance = 0.4f;
  [Range(0f, 1f)]
  public float chanceDecreasePerSoul = 0.1f;

  void Awake() {
    AwakeSingleton();
  }

  float GetDropChance() {
    int allyCount = EnemyManager.instance.allies.Count;
    float rawChance = initialChance - (chanceDecreasePerSoul * (soulCount + allyCount));
    return Mathf.Clamp(rawChance, 0f, 1f);
  }

  public bool ShouldDropRandomly() {
    if (soulCount >= maxSoulsAtOnce) {
      return false;
    }

    float chance = GetDropChance();
    return (Random.Range(0f, 1f) <= chance);
  }

  public void AddSoul() {
    soulCount++;
  }

  public void RemoveSoul() {
    soulCount--;
  }
}

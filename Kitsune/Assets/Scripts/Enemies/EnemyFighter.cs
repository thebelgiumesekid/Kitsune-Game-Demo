using UnityEngine;

[RequireComponent(typeof(HitBoxCombat))]
public class EnemyFighter : EnemyController {
  HitBoxCombat hitBoxCombat;

  AudioSource source;
  public AudioClip attackSound;

  protected override void Awake() {
    base.Awake();
    hitBoxCombat = GetComponent<HitBoxCombat>();
    source = GetComponent<AudioSource>();
  }

  protected override void StartAttack(Transform target) {
    CharacterStats targetStats = target.GetComponent<CharacterStats>();
    if (combat.Attack(targetStats)) {
      moveController.SetPaused(true);
    }
  }

  public override void AttackAt(Transform target) {
    base.AttackAt(target);
    Collider[] hitByAttack = hitBoxCombat.GetHitByAttack();
    moveController.SetPaused(false);
    source.PlayOneShot(attackSound);

    foreach (Collider targetCollider in hitByAttack) {
      if (CanHitWithTag(targetCollider.gameObject.tag)) {
        CharacterStats targetStats = targetCollider.GetComponent<CharacterStats>();
        if (targetStats != null) {
          hitBoxCombat.Damage(targetStats);
        }
      }
    }
  }
}
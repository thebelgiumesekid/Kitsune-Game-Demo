﻿using UnityEngine;

/* Has a chance to drop a soul on death */
[RequireComponent(typeof(EnemyController))]
public class SoulDropper : MonoBehaviour {
  public GameObject soulPrefab;

  void Start() {
    EnemyController controller = GetComponent<EnemyController>();
    controller.OnDestroy += OnDeath;
  }

  void OnDeath() {
    if (SoulDropManager.instance.ShouldDropRandomly()) {
      SoulDropManager.instance.AddSoul();
      DropSoul();
    }
  }

  void DropSoul() {
    if (soulPrefab != null) {
      Instantiate(soulPrefab, transform.position, Quaternion.identity, SoulDropManager.instance.transform);
    }
  }
}

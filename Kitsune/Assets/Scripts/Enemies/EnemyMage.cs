﻿using UnityEngine;

[RequireComponent(typeof(HitBoxCombat))]
public class EnemyMage : EnemyController {
  float nextAttack = 0f;
  bool isAttacking = false;
  HitBoxCombat hitBoxCombat;
  // ParticleSystem warmupEffects;
  // ParticleSystem attackEffects;

  [SerializeField] ParticleSystem warmupEffects = null;
  [SerializeField] ParticleSystem attackEffects = null;
  [SerializeField] Vector3 effectTransform = Vector3.zero;
  [SerializeField] Vector3 effectRotation = Vector3.zero;

  [SerializeField] float attackHoldTime = 1f;
  [SerializeField] float attackDamageDelay = 0.2f;
  [SerializeField] float attackToMoveDelay = 0.5f;

  public event System.Action OnAttackFinish;

  protected override void Awake() {
    base.Awake();
    hitBoxCombat = GetComponent<HitBoxCombat>();
  }

  // protected override void Start() {
  //   base.Start();
  //   warmupEffects = Instantiate(warmupEffectsPrefab);
  //   attackEffects = Instantiate(attackEffectsPrefab);
  // }

  // void OnDestroy() {
  //   Destroy(warmupEffects);
  //   Destroy(attackEffects);
  // }

  protected override void StartAttack(Transform target) {
    CharacterStats targetStats = target.GetComponent<CharacterStats>();
    if (combat.Attack(targetStats)) {
      moveController.SetPaused(true);
      Invoke("StartEffects", attackToMoveDelay);
    }
  }

  public override void AttackAt(Transform target) {
    base.AttackAt(target);
    warmupEffects.Stop();
    attackEffects.Play();

    isAttacking = true;

    Invoke("AttackFinish", attackHoldTime);
  }

  void StartEffects() {
    warmupEffects.Play();
  }

  void ResumeMovement() {
    moveController.SetPaused(false);
  }

  void PlayEffect(ParticleSystem effect) {
    effect.transform.position = transform.TransformPoint(effectTransform);
    effect.transform.rotation = transform.rotation * Quaternion.Euler(effectRotation);

    effect.Play();
  }

  void Update() {
    if (!isAttacking) {
      return;
    }

    float curTime = Time.time;
    if (curTime > nextAttack) {
      Collider[] hitByAttack = hitBoxCombat.GetHitByAttack();
      foreach (Collider target in hitByAttack) {
        if (CanHitWithTag(target.gameObject.tag)) {
          CharacterStats targetStats = target.GetComponent<CharacterStats>();
          if (targetStats != null) {
            combat.Damage(targetStats);
            nextAttack = curTime + attackDamageDelay;
          }
        }
      }
    }
  }

  void AttackFinish() {
    isAttacking = false;
    attackEffects.Stop();


    if (OnAttackFinish != null) {
      OnAttackFinish();
    }
    Invoke("ResumeMovement", attackToMoveDelay);
  }
}

using System.Collections;
using UnityEngine;

/* Controls an arrow's movement and collision events */

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Arrow : MonoBehaviour, IPooledObject {
  new Rigidbody rigidbody;
  new Collider collider;

  [SerializeField] float lifetime = 20f;

  public delegate void OnArrowHit(Collider collider, Arrow arrow);
  public OnArrowHit onArrowHitCallback;

  void Awake() {
    rigidbody = GetComponent<Rigidbody>();
    collider = GetComponent<Collider>();
  }

  void Start() {
    OnObjectSpawn();
  }

  void Update() {
    if (rigidbody.velocity != Vector3.zero) {
      transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
    }
  }

  public void SetVelocity(float velocity) {
    float force = velocity * rigidbody.mass;
    rigidbody.AddForce(transform.forward * force, ForceMode.Impulse);
  }

  public void Destroy() {
    Destroy(this.gameObject);
  }

  public void OnObjectSpawn() {
    CancelInvoke();
    Invoke("Destroy", lifetime);
    collider.enabled = true;
    rigidbody.isKinematic = false;
    rigidbody.useGravity = true;
    rigidbody.velocity = Vector3.zero;
  }

  public void Stick() {
    collider.enabled = false;

    rigidbody.isKinematic = true;
    rigidbody.useGravity = false;
    rigidbody.velocity = Vector3.zero;
  }

  void OnCollisionEnter(Collision collision) {
    if (onArrowHitCallback != null) {
      TriggerCallback(collision.collider);
    }
  }

  void OnTriggerEnter(Collider collider) {
    if (onArrowHitCallback != null) {
      TriggerCallback(collider);
    }
  }

  void TriggerCallback(Collider collider) {
    onArrowHitCallback(collider, this);
  }
}
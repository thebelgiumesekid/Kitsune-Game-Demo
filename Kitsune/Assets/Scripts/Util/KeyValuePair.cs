// [System.Serializable]
using UnityEngine;


public class KeyValuePair<A, B> {
  [SerializeField] public A key;
  [SerializeField] public B value;
}
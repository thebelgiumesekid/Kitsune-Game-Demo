using UnityEngine;

/* Controls what happens when the player tries to interact with something */

public class PlayerInteraction : MonoBehaviour {
  PlayerInventory playerInventory;

  [SerializeField] Vector3 interactPosition = Vector3.zero;
  [SerializeField] float interactRadius = 0.5f;

  void Awake() {
    playerInventory = GetComponent<PlayerInventory>();
  }

  void Update() {
    if (Input.GetKeyDown(KeyCode.E)) {
      Interact();
    }
  }

  void OnDrawGizmosSelected() {
    Vector3 localOffset = transform.TransformPoint(interactPosition);
    Gizmos.color = Color.yellow;
    Gizmos.DrawWireSphere(localOffset, interactRadius);
  }

  void ProcessColliderInteractions(Collider[] colliders) {
    foreach (Collider collider in colliders) {
      if (TryUsePickup(collider.gameObject)) {
        return;
      }

      Interactable interactable = collider.GetComponent<Interactable>();
      if (interactable != null && TryInteract(interactable)) {
        return;
      }
    }

    // No valid interactions, drop held item if it exists
    playerInventory.DropItem();
  }

  private bool TryInteract(Interactable interactable) {
    return interactable.TryInteract(transform);
  }

  private bool TryUsePickup(GameObject interactable) {
    if (!playerInventory.HasItem()) {
      return false;
    }

    return playerInventory.UseItem(interactable);
  }

  public void Interact() {
    Vector3 localOffset = transform.TransformPoint(interactPosition);
    Collider[] colliders = Physics.OverlapSphere(localOffset, interactRadius);

    ProcessColliderInteractions(colliders);
  }
}
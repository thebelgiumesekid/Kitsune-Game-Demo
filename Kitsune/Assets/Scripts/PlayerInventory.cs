using UnityEngine;

/* Allows to player to pick up and drop Pickup items */

public class PlayerInventory : MonoBehaviour {
  Pickup heldItem;
  float gizmoSphereRadius = 0.05f;

  [SerializeField] Transform holdRoot = null;
  [SerializeField] Vector3 heldItemPosition = Vector3.zero;
  [SerializeField] Vector3 dropItemPosition = Vector3.zero;

  void Awake() {
    if (holdRoot == null) {
      holdRoot = transform;
    }
  }

  void OnDrawGizmosSelected() {
    Gizmos.color = Color.blue;
    Transform holdTransform = (holdRoot == null) ? transform : holdRoot;
    Gizmos.DrawWireSphere(holdTransform.TransformPoint(heldItemPosition), gizmoSphereRadius);
    Gizmos.DrawWireSphere(transform.TransformPoint(dropItemPosition), gizmoSphereRadius);
  }

  public void HoldItem(Pickup newItem) {
    if (HasItem()) {
      DropItem();
    }

    heldItem = newItem;
    PositionHeldItem();
  }

  public void DropItem() {
    if (heldItem != null) {
      heldItem.UnsetHeld();

      heldItem.transform.parent = null;
      heldItem.transform.position = transform.TransformPoint(dropItemPosition);
      heldItem = null;
    }
  }

  public Pickup GetItem() {
    return heldItem;
  }

  public bool HasItem() {
    return heldItem != null;
  }

  public bool UseItem(GameObject target) {
    if (heldItem != null) {
      return heldItem.UseOn(target);
    }

    return false;
  }

  void PositionHeldItem() {
    heldItem.transform.parent = holdRoot;
    heldItem.transform.localPosition = heldItemPosition;

    heldItem.SetHeld();
  }
}
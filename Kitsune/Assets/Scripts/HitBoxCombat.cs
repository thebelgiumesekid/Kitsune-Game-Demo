using UnityEngine;

public class HitBoxCombat : CharacterCombat {
  public enum Shape { Box, Sphere };

  [Header("Attack Settings")]
  [SerializeField] Vector3 attackPosition = Vector3.zero;
  [SerializeField] Shape shape = Shape.Sphere;
  [SerializeField] float attackRadius = 0.5f;
  [SerializeField] Vector3 attackVolume = Vector3.zero;

  public Collider[] GetHitByAttack() {
    Vector3 localOffset = transform.TransformPoint(attackPosition);
    if (shape == Shape.Sphere) {
      return Physics.OverlapSphere(localOffset, attackRadius);
    } else {
      return Physics.OverlapBox(localOffset, attackVolume, transform.rotation);
    }
  }

  void OnDrawGizmosSelected() {
    Gizmos.color = Color.red;
    Gizmos.matrix = this.transform.localToWorldMatrix;
    // Vector3 localOffset = transform.TransformPoint(attackPosition);

    if (shape == Shape.Sphere) {
      Gizmos.DrawWireSphere(attackPosition, attackRadius);
    } else {
      Gizmos.DrawWireCube(attackPosition, attackVolume);
    }
  }
}
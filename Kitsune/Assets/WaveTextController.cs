﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class WaveTextController : MonoBehaviour {
  WaveController waveController;

  public Text textUI;
  public float fadeInTime = 1f;
  public float fadeOutTime = 2f;
  public float onScreenTime = 3f;
  public List<string> waveText;

  void Start() {
    waveController = GetComponent<WaveController>();
    waveController.NextWave += OnNextWave;
  }

  void OnNextWave() {
    int i = waveController.getCurrentWave();
    if (i < waveText.Count) {
      FadeInText(waveText[i]);
    }
  }

  void FadeInText(string text) {
    if (textUI == null) {
      return;
    }
    textUI.text = text;

    textUI.CrossFadeAlpha(0f, 0f, true);
    textUI.CrossFadeAlpha(1f, fadeInTime, true);

    Invoke("FadeOutText", onScreenTime + fadeInTime);
  }

  void FadeOutText() {
    textUI.CrossFadeAlpha(0f, fadeOutTime, true);
  }
}
